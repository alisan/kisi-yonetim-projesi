<?php
namespace App\Repositories;

use App\Exceptions\GeneralException;
use App\Models\Person;
use App\Models\Address;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB as DB;

/**
 * Class BlogsRepository.
 */
class PersonRepository extends BaseRepository
{
    const MODEL = Person::class;

    private $indexCacheKey = 'people_page_';

    private $tmpItemIds = [];

    public function getAllWithPaginate($page)
    {
        if (Cache::has($this->indexCacheKey . $page)) {

            return Cache::get($this->indexCacheKey . $page);
        } else {

            return Cache::remember('people_page_' . $page, 3, function() {
                return $this->query()
                    ->select([
                        config('module.person.table').'.id',
                        config('module.person.table').'.name',
                        config('module.person.table').'.birthday',
                        config('module.person.table').'.gender',
                        config('module.person.table').'.created_at',
                    ])
                    ->paginate(10);
            });
        }
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.person.table').'.id',
                config('module.person.table').'.name',
                config('module.person.table').'.birthday',
                config('module.person.table').'.gender',
                config('module.person.table').'.created_at',
            ]);
    }

    /**
     * @param array $input
     *
     * @return
     */
    public function create(array $input)
    {
        $addressesArray = isset($input['addresses']) ? $input['addresses'] : [];
        $addressesWithModel = collect($addressesArray)->map(function ($q) {
           return new Address($q);
        });

        unset($input['addresses']);

        $dbSave = DB::transaction(function () use ($input, $addressesArray, $addressesWithModel) {

            if ($person = Person::create($input)) {
                // Inserting associated category's id in mapper table
                if (count($addressesArray) > 0) {

                    $person->addresses()->saveMany($addressesWithModel);
                }
                return $person;
            }
            throw new GeneralException("Kişi eklerken hata oluştur");
        });

        return $dbSave;
    }

    /**
     * Update Blog.
     *
     * @param \App\Models\Person $person
     * @param array $input
     * @return
     */
    public function update(Person $person, array $input)
    {
        $addressesArray = isset($input['addresses']) ? $input['addresses'] : [];
        $id = $person->id;
        $addressesWithModel = collect($addressesArray)->map(function ($q) use ($id) {
            if (isset($q['id'])) {
                Address::where('id', $q['id'])
                    ->where('person_id', $id)
                    ->update($q);
                $this->tmpItemIds[] = $q['id'];
                return null;
            } else {
                return new Address($q);
            }
        })->reject(function ($item) {
            return is_null($item);
        });

        unset($input['addresses']);

        $updatePerson = DB::transaction(function () use ($person, $input, $addressesArray, $addressesWithModel) {
            if ($person->update($input)) {

                // delete removed items
                if(count($this->tmpItemIds)) {
                    Address::where('person_id', $person->id)
                        ->whereNotIn('id', $this->tmpItemIds)
                        ->delete();
                }

                // save new items
                if (count($addressesArray) > 0) {
                    $person->addresses()->saveMany($addressesWithModel);
                }

                return $person;
            }
            throw new GeneralException(
                "Kişi güncellemesi işleminde hata oluştu!"
            );
        });

        return $updatePerson;
    }

    public function showWithAddresses(Person $person)
    {
        return $person->with('addresses');
    }

    /**
     * @param \App\Models\Person $person
     *
     * @throws GeneralException
     *
     * @return bool
     */
    public function delete(Person $person)
    {
        DB::transaction(function () use ($person) {
            if ($person->delete()) {
                Address::where('person_id', $person->id)->delete();
                return true;
            }
            throw new GeneralException("Kişi silme işlemi esnasında hata oluştu");
        });
    }
}