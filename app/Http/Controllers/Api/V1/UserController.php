<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\UserResource;
use JWTAuth;
use Illuminate\Http\Request;

class UserController extends ApiController
{
    public function showUserInformation()
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            return $this->respond(['message' => $e->getMessage()]);
        }

        return new UserResource($user);
    }
}
