<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\PersonResource;
use App\Models\Person;
use App\Http\Requests\StorePeopleRequest;
use App\Http\Requests\UpdatePeopleRequest;
use App\Repositories\PersonRepository;
use Illuminate\Http\Request;

/**
 * Class PeopleController
 * @package App\Http\Controllers\Api\V1
 */
class PeopleController extends ApiController
{
    protected $repository;
    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(PersonRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $limit = $request->get('paginate') ? $request->get('paginate') : 25;
        $orderBy = $request->get('orderBy') ? $request->get('orderBy') : 'ASC';
        $sortBy = $request->get('sortBy') ? $request->get('sortBy') : 'created_at';
        return PersonResource::collection(
            $this->repository->getForDataTable()->orderBy($sortBy, $orderBy)->paginate($limit)
        );
    }

    /**
     * @param Person $person
     * @return PersonResource
     */
    public function show(Person $person)
    {
        return new PersonResource($person);
    }

    /**
     * @param StorePeopleRequest $request
     * @return PersonResource
     * @throws \App\Exceptions\GeneralException
     */
    public function store(StorePeopleRequest $request)
    {
        $person = $this->repository->create($request->all());

        return new PersonResource($person);
    }

    /**
     * @param UpdatePeopleRequest $request
     * @param Person $person
     * @return PersonResource
     */
    public function update(UpdatePeopleRequest $request, Person $person)
    {
        $person = $this->repository->update($person, $request->all());

        return new PersonResource($person);
    }

    /**
     * @param Person $person
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Person $person)
    {
        $this->repository->delete($person);

        return $this->respond([
            'status' => 'ok',
            'message' => 'Kişi silindi.',
        ]);
    }
}
