<?php

namespace App\Http\Resources;

use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class PersonResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @param bool $index
     * @return array
     */
    public function toArray($request)
    {
        $responseData = [
            'id'                => $this->id,
            'name'              => $this->name,
            'birthday'          => $this->birthday,
            'birthday_view'     => Carbon::createFromFormat('Y-m-d', $this->birthday)->format(config('app.date_format')),
            'gender_view'       => Person::$enum_gender[$this->gender],
            'gender'            => $this->gender,
            'created_at'        => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format(config('app.date_format')),
        ];
        if (!empty($request->route()->parameters()) && isset($request->route()->parameters()['person'])) {
            $responseData['addresses'] = $this->addresses;
        }
        return $responseData;
    }
}