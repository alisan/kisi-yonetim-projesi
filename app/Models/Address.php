<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Address
 *
 * @package App
 * @property string $address
 * @property string $post_code
 * @property string $city_name
 * @property string $country_name
 * @property string $person
 */
class Address extends Model
{
    use SoftDeletes;

    protected $table = 'addresses';

    public $timestamps = true;

    protected $dates = ['deleted_at'];

    protected $fillable = ['address', 'post_code', 'city_name', 'country_name', 'person_id'];

    /**
     * Set to null if empty
     * @param $input
     */
    public function setPersonIdAttribute($input)
    {
        $this->attributes['person_id'] = $input ? $input : null;
    }

    public function person()
    {
        return $this->belongsTo(Person::class, 'person_id');
    }

}