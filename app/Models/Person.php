<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Person
 *
 * @package App\Models
 * @property string $name
 * @property string $birthday
 * @property enum $gender
 */
class Person extends Model
{

    protected $table = 'people';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'birthday', 'gender'];


    public static $enum_gender = [
        "F" => "Kadın",
        "M" => "Erkek"
    ];

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setBirthdayAttribute($input)
    {
        if ($input) {
            $this->attributes['birthday'] = Carbon::createFromFormat(config('app.date_format_db'), $input)->format('Y-m-d');
        }
    }

    /**
     * Get attribute from date format
     * @param $output
     *
     * @return string
     */
    /*public function getBirthdayAttribute($output)
    {
        if ($output) {
            return Carbon::createFromFormat('Y-m-d', $output)->format(config('app.date_format'));
        }
    }*/

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

}