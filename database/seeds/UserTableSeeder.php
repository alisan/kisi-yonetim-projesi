<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$/yJ1yTg8Q0KPgKj./1X5eOC3o.ayT1DXwnbDg1mV9kqkKlsHrGj.i', 'remember_token' => '',],

        ];

        foreach ($items as $item) {
            \App\Models\User::create($item);
        }
    }
}
