<?php

use App\Repositories\PersonRepository;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PersonTableSeeder extends Seeder
{
    protected $repository;

    /**
     * __construct.
     *
     * @param $repository
     */
    public function __construct(PersonRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $enumGenders = \App\Models\Person::$enum_gender;

        foreach(range(1, 40) as $index)
        {
            $start_date = '1990-12-31 00:00:00';
            $end_date = '1990-01-01 00:00:00';

            $min = strtotime($start_date);
            $max = strtotime($end_date);

            // Generate random number using above bounds
            $val = rand($min, $max);

            // Convert back to desired date format
            $start = new \DateTime(date('Y-m-d', $val));
            $characters = 'FM';

            $data = [
                'name'       => $faker->name(),
                'gender'     => $characters[rand(0, 1)],
                'birthday'   => $start->format('Y-m-d'),
                'addresses' => [
                    [
                        'address' => $faker->address(),
                        'city_name' => $faker->city(),
                        'country_name' => $faker->country(),
                        'post_code' => $faker->postcode(),
                    ]
                ]
            ];

            $this->repository->create($data);
        }
    }
}
