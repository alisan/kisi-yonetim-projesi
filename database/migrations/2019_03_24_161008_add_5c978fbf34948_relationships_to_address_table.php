<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class Add5c978fbf34948RelationshipsToAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function(Blueprint $table) {
            if (!Schema::hasColumn('addresses', 'person_id')) {
                $table->integer('person_id')->unsigned()->nullable();
                $table->foreign('person_id', '32779_5c978fbead16c')->references('id')->on('people')->onDelete('cascade');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function(Blueprint $table) {
            if (Schema::hasColumn('addresses', 'person_id')) {
                $table->dropForeign('32779_5c978fbead16c');
                $table->dropColumn('person_id');
            }
        });
    }
}
