<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/people', 'PeopleController@index')->name('web.people');
Route::get('/people/{id}', 'PeopleController@show')->name('web.people.show');

Route::get('/admin', function () {
    return view('admin/app');
});

Route::get('/login', function () {
    return view('admin/app');
});

Route::get('/admin/{any}', function () {
    return view('admin/app');
})->where('any', '.*');

