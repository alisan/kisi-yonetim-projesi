@extends('web.layout')

@section('head')
    {!! SEO::generate() !!}
@endsection

@section('content')
    <h1>Kişi Listeleme</h1>

    <table>
        <tbody>
        @foreach($people as $person)
            <tr>
                <td>{{$person->name}}</td>
                <td>{{$person->birthday_view}}</td>
                <td>{{$person->gender_view}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$people}}
@endsection