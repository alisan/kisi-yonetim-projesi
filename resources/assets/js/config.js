export default {
    API_URL: 'http://localhost:8081/api/v1',
    TOKEN_KEY: 'TOKEN',
    ajaxHeaders: {
        'Accept': 'application/json, charset=utf8',
        'Content-type': 'application/vnd.api+json, charset=utf8',
        'X-Requested-With': 'XMLHttpRequest',
    },
    params: params => '?' + Object.entries(params).map(o => o.join('=')).join('&'),
};
