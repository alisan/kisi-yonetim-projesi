
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';

import Routes from '@/js/routes';
import config from '@/js/config';
import store from '@/js/store';
import DatePicker from '@/js/components/Common/Date';
import Alert from '@/js/components/Common/Alert';

import App from '@/js/views/App';

Vue.use(Vuetify);
Vue.component('v-date', DatePicker);
Vue.component('v-confirm', Alert);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Routes.beforeEach(async (to, from, next) => {
    if (to.meta.requiresAuth) {
        if (!!localStorage[config.TOKEN_KEY]) {
            if (Object.keys(store.state.Auth.user).length === 0 && to.name !== 'init') {
                next({
                    name: 'init',
                    query: {redirect: to.fullPath},
                })
            } else {
                next()
            }
        } else {
            next({
                name: 'signIn',
                query: {redirect: to.fullPath},
            });
        }
    } else {
        if (!!localStorage[config.TOKEN_KEY]) {
            next('/admin');
        }
        next();
    }
});

const app = new Vue({
    el: '#app',
    router: Routes,
    store,
    render: h => h(App),
});

export default app;
