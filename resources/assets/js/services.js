export default ({
    auth: {
        login: 'login',
    },
    me: 'me',
    people: 'people',
});
