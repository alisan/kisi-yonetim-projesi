import Vuex from 'vuex';
import axios from 'axios';
import router from '../routes';
import config from '../config';
import services from '../services'

const request = (store, config) => axios.create({
    headers: {
        common: (() => {
            const token = store.getters['Auth/TOKEN'];
            if (!!token) config.ajaxHeaders['Authorization'] = token;
            return {
                ...config.ajaxHeaders
            }
        })(),
    },
});

function isPromise(val) {
    return val && typeof val.then === 'function';
}

function patchAction(store, type, handler, local) {
    const entry = store._actions[type] || (store._actions[type] = []);
    if (entry.length > 0) entry.pop();
    const http = request(store, config);

    http.interceptors.response.use((response) => {
        return response;
    }, function (error) {
        if (error.response.status === 422) {
            store._vm.$bus.$emit('errors', error.response.data);
        }
        return Promise.reject(error.response);
    });

    entry.push(function wrappedActionHandler(payload, cb) {
        let res = handler({
            dispatch: local.dispatch,
            commit: local.commit,
            getters: local.getters,
            state: local.state,
            rootGetters: store.getters,
            rootState: store.state,
            rootDispatch: store.dispatch,

            router,
            services,
            config,
            http,
        }, payload, cb);
        if (!isPromise(res)) {
            res = Promise.resolve(res);
        }
        if (store._devtoolHook) {
            return res.catch(err => {
                store._devtoolHook.emit('vuex:error', err);
                throw err;
            });
        } else {
            return res;
        }
    });
}

// Based on installModule from vuex/src/store.js
function patchModule(store, path, module, hot) {
    const namespace = store._modules.getNamespace(path);

    const local = module.context;

    module.forEachAction((action, key) => {
        const namespacedType = namespace + key;
        patchAction(store, namespacedType, action, local);
    });

    module.forEachChild((child, key) => {
        patchModule(store, path.concat(key), child, hot);
    });

}

export default function axiosStorePlugin() {
    return store => {
        // Patch all current modules to include $axios parameter in action handlers.
        patchModule(store, [], store._modules.root);

        // Patch registerModule to auto patch newly added dynamic modules
        const orig = Vuex.Store.prototype.registerModule;
        Vuex.Store.prototype.registerModule = function registerModule(path, rawModule) {
            orig.call(this, path, rawModule);
            patchModule(this, path, this._modules.get(path));
        };
    };
}
