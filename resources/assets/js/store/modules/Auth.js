import config from '../../config.js';

export default {
    state: {
        token: localStorage[config.TOKEN_KEY] || '',
        user: {},
        users: {},
        loading: false,
        fail: false,
        fetching: [],
        success: null
    },

    getters: {
        TOKEN: ({token}) => `Bearer ${token}`,
        FAIL: ({fail}) => fail,
        USER: ({user}) => user,
        USERS: ({users}) => users,
        SUCCESS: ({success}) => success,
        LOADING: ({loading}) => loading,
    },

    mutations: {
        SET_TOKEN: (state, token) => {
            state.token = token;
            localStorage[config.TOKEN_KEY] = token;
        },
        SET_USER: (state, user) => state.user = user,
        SET_USERS: (state, users) => state.users = items,
        SET_LOADING: (state, loading) => state.loading = loading,
        SET_FAIL: (state, fail) => state.fail = fail,
        SET_SUCCESS: (state, success) => state.success = success,
    },

    actions: {
        async FETCH_USER({commit, http, router, services}) {
            try {
                let {data: {data}} = await http.get(`${config.API_URL}/${services.me}`)
                data = {
                    ...data,
                };
                commit('SET_USER', data)
            } catch (e) {
                throw e
            }
        },
        async LOGIN({commit, http, router, services, rootDispatch}, {email, password}) {
            try {
                const {auth: {login}} = services;
                commit('SET_LOADING', true);
                commit('SET_FAIL', false);
                const {data} = await http.post(`${config.API_URL}/${login}`, {email, password});
                if (data.error) {
                    commit('SET_LOADING', false);
                    commit('SET_FAIL', true);
                } else {
                    const {token, ...user} = data;
                    commit('SET_LOADING', false);
                    commit('SET_TOKEN', data.token);
                    commit('SET_USER', user);
                    window.location.href = "/admin"
                }
                // router.push({
                //   name: 'init',
                // });
            } catch (e) {
                commit('SET_LOADING', false);
                commit('SET_FAIL', true);
                throw e;
            }
        },
    },
};
