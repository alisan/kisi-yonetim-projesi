export default {

    state: {
        items: [],
        item: {},
        loading: false,
        rows: 0,
        total: 2,
    },

    getters: {
        ITEMS: ({items}) => items,
        ITEM: ({item}) => item,
        ROWS: ({rows}) => rows,
        LOADING: ({loading}) => loading,
        TOTAL: ({total}) => total,
    },

    mutations: {
        CLEAR_ITEMS: (state) => state.items = [],
        SET_ITEMS: (state, items) => state.items = items,
        SET_ITEM: (state, item) => state.item = item,
        SET_ROWS: (state, rows) => state.rows = rows,
        SET_LOADING: (state, loading) => state.loading = loading,
        SET_TOTAL: (state, total) => state.total = total,
    },

    actions: {
        async FETCH_PEOPLE({commit, http, rootGetters, config, services}, params) {
            commit('SET_LOADING', true);
            commit('SET_ITEMS', []);
            try {
                http.defaults.headers.common['Authorization'] = rootGetters['Auth/TOKEN'];
                const {data} = await http.get(`${config.API_URL}/${services.people}${config.params(params)}`);

                const items = data.data.map(e => ({
                    ...e,
                }));

                commit('SET_TOTAL', data.meta.last_page);
                commit('SET_ROWS', data.meta.total);
                commit('SET_ITEMS', items);
                commit('SET_LOADING', false);

            } catch (e) {
                commit('SET_LOADING', false);

                throw e
            }
        },
        async POST({commit, http, config, services, router}, form) {
            try {
                commit('SET_LOADING', true);
                let formData = {
                    ...form,
                }

                const {data: {id}} = await http.post(`${config.API_URL}/${services.people}`, formData)
                commit('SET_LOADING', false);
                router.push({
                    name: 'peopleDetail',
                    params: {
                        id,
                    }
                })
            } catch (e) {
                commit('SET_LOADING', false);
                throw e
            }
        },
        async FIND({http, commit, config, services}, id) {
            try {
                commit('SET_LOADING', true);
                const {data: {data}} = await http.get(`${config.API_URL}/${services.people}/${id}`);
                let item = {
                    ...data,
                }

                commit('SET_LOADING', false);
                commit('SET_ITEM', item)
            } catch (e) {
                commit('SET_LOADING', false);
                throw e
            }
        },
        async DELETE({http, commit, config, services, router, dispatch}, id) {
            try {
                commit('SET_LOADING', true);
                const {data: {data}} = await http.delete(`${config.API_URL}/${services.people}/${id}`);
                router.push({
                    name: 'people',
                    params: {
                        page: 1
                    }
                })
            } catch (e) {
                commit('SET_LOADING', false);
                router.push({
                    name: 'people',
                    params: {
                        page: 1
                    }
                });
                throw e
            }
        },
        async EDIT({commit, http, config, services, router}, {id, form}) {

            commit('SET_LOADING', true);
            let formData = {
                ...form,
            }

            try {
                await http.put(`${config.API_URL}/${services.people}/${id}`, formData);
                commit('SET_LOADING', false);
                router.push({
                    name: 'peopleDetail',
                    params: {
                        id,
                    }
                })
            } catch (e) {
                commit('SET_LOADING', false);
                throw e
            }
        },
    }
};
