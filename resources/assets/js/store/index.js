import Vue from 'vue';
import Vuex from 'vuex';
import axiosPlugin from './axiosPlugin';
import modules from './modules';

Vue.use(Vuex);
export default new Vuex.Store({
    modules,
    state: {
        LOADING: false,
        ERRORS: [],
    },
    mutations: {
        SET_LOADING: (state, status = false) => state.LOADING = status,
    },
    actions: {

    },
    getters: {
        ERRORS: ({ERRORS}) => ERRORS,
        LOADING: ({LOADING}) => LOADING,
    },

    plugins: [axiosPlugin()],
    // strict: process.env.NODE_ENV !== 'production',
});
