import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/js/pages/Home';
import People from '@/js/pages/People/List';
import PeopleAdd from '@/js/pages/People/Add';
import PeopleEdit from '@/js/pages/People/Edit';
import peopleDetail from '@/js/pages/People/Detail';
import Init from './pages/Init.vue';
import Auth from '@/js/pages/Auth';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin',
            name: 'home',
            component: Home,
            meta: {
                title: 'Dashboard',
                open: true,
                icon: 'home',
                requiresAuth: true,
            }
        },
        {
            path: '/login',
            component: Auth,
            name: 'signIn',
            meta: {
                requiresAuth: false,
            },
        },
        {
            path: '/init',
            component: Init,
            name: 'init',
            meta: {
                requiresAuth: true,
            },
        },
        {
            path: '/admin/people',
            name: 'people',
            component: People,
            meta: {
                title: 'Kişiler',
                open: true,
                icon: 'widgets',
                requiresAuth: true,
            }
        },
        {
            path: '/admin/people/add',
            name: 'peopleAdd',
            component: PeopleAdd,
            meta: {
                title: 'Kişi Ekleme',
                open: false,
                icon: 'widgets',
                requiresAuth: true,
            }
        },
        {
            path: '/admin/people/edit/:id',
            name: 'peopleEdit',
            component: PeopleEdit,
            meta: {
                title: 'Kişi Düzenleme',
                open: false,
                icon: 'widgets',
                requiresAuth: true,
            }
        },
        {
            path: '/admin/people/view/:id',
            name: 'peopleDetail',
            component: peopleDetail,
            meta: {
                title: 'Kişi Detay',
                open: false,
                icon: 'widgets',
                requiresAuth: true,
            }
        }
    ]
});

/*function requireAuth (to, from, next) {
    if (window.User) {
        return next()
    } else {
        return next('/')
    }
}*/
export default router;