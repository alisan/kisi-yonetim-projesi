<?php

namespace Tests\Feature\Http\Controllers\Api\V1;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Person;
use Illuminate\Support\Facades\Hash;

class PeopleControllerTest extends TestCase
{
    use DatabaseMigrations;

    private $person;
    private $user;
    private $token;

    public function setUp()
    {
        parent::setUp();
        $user = new User([
            'name' => 'Alisan',
            'email' => 'alisan@email.com',
            'password' => Hash::make('123456')
        ]);
        $user->save();

        $this->user = $user;
        $this->person = [
            'name' => 'Alisan',
            'birthday' => '1990-12-05',
            'gender' => 'M',
            'addresses' => [
                [
                    'address' => 'adresim',
                    'post_code' => '3555',
                    'country_name' => 'Türkiye',
                    'city_name' => 'istanbul'
                ],
                [
                    'address' => 'adresim 2',
                    'post_code' => '3455',
                    'country_name' => 'Türkiye',
                    'city_name' => 'Antalya'
                ]
            ]
        ];
        $this->getToken();
    }

    /**
     * success test
     */
    public function testIndexSuccessfully()
    {
        $this->get('api/v1/people', [
            'Authorization' => 'Bearer ' . $this->token
        ])->assertJsonStructure([
            'meta',
        ])->isOk();
    }

    /**
     * error test without authentication
     */
    public function testIndexWithAuthError()
    {
        $this->post('api/v1/people', [
        ])->assertJsonStructure([
            'error',
        ])->assertStatus(400);
    }

    /**
     * success test
     */
    public function testCreatePeople()
    {
        $this->post('api/v1/people', $this->person, [
            'Authorization' => 'Bearer ' . $this->token
        ])->assertJsonStructure([
            'data'
        ])->isOk();
    }

    /**
     * success test
     */
    public function testEditPeople()
    {
        $id = $this->createPeople();
        $this->person['name'] = 'Ahmet';
        $this->put('api/v1/people/'.$id, $this->person, [
            'Authorization' => 'Bearer ' . $this->token
        ])->assertJson([
            'data' => [
                'name' => "Ahmet"
            ]
        ])->isOk();
    }

    public function testDeletePeople()
    {
        $id = $this->createPeople();
        $this->delete('api/v1/people/'.$id, [
            'Authorization' => 'Bearer ' . $this->token
        ])->assertJson([
            'status' => 'ok'
        ])->isOk();
    }

    /**
     * get token
     */
    private function getToken()
    {
        $response = $this->post('api/v1/login', [
            'email' => 'alisan@email.com',
            'password' => '123456'
        ]);
        $response->assertStatus(200);
        $responseJSON = json_decode($response->getContent(), true);
        $token = $responseJSON['token'];
        $this->token = $token;
    }

    private function createPeople()
    {
        $response = $this->post('api/v1/people', $this->person, [
            'Authorization' => 'Bearer ' . $this->token
        ]);
        $response->assertStatus(201);
        $responseJSON = json_decode($response->getContent(), true);
        $id = $responseJSON['data']['id'];
        return $id;
    }
}
