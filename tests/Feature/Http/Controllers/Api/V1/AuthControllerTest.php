<?php

namespace Tests\Feature\Http\Controllers\Api\V1;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $user = new User([
            'name' => 'Alisan',
            'email' => 'alisan@email.com',
            'password' => Hash::make('123456')
        ]);
        $user->save();
    }

    /**
     * success test
     */
    public function testLoginSuccessfully()
    {
        $this->post('api/v1/login', [
            'email' => 'alisan@email.com',
            'password' => '123456'
        ])->assertJsonStructure([
            'token',
        ])->isOk();
    }

    /**
     * wrong test
     */
    public function testLoginWithReturnsWrongCredentialsError()
    {
        $this->post('api/v1/login', [
            'email' => 'yanlis@email.com',
            'password' => '123456'
        ])->assertJsonStructure([
            'error'
        ])->assertStatus(401);
    }

    /**
     * validation test
     */
    public function testLoginWithReturnsValidationError()
    {
        $this->post('api/v1/login', [
            'email' => 'test@email.com'
        ])->assertStatus(422);
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
