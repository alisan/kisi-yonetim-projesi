<?php
return [
    'person' => [
        'table' => 'people',
    ],
    'address' => [
        'table' => 'addresses',
    ],
];