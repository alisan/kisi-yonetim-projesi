FROM alisanalacam/base-app-php7.1-debian:latest
COPY . /web
COPY ./deploy/.env /web/.env
#RUN cp /web/deploy/.env /web/.env
#RUN cp /web/deploy/php_include_conf.ini /etc/php/7.2/fpm/conf.d/php_include_conf.ini
RUN sed -i -e "s/;clear_env\s*=\s*no/clear_env = no/g" /etc/php/7.2/fpm/pool.d/www.conf
WORKDIR /web
RUN /usr/bin/composer install
RUN npm install
RUN chmod -R 777 storage
RUN chmod -R 777 bootstrap
RUN cp deploy/nginx.conf /etc/nginx/sites-enabled/default
RUN /etc/init.d/php7.2-fpm restart
CMD /etc/init.d/php7.2-fpm start && nginx -g "daemon off;"