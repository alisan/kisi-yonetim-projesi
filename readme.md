## Kişi Yönetim Uygulaması

Uygulama hakkında bilgiler buraya yazılacaktır.

Geliştirme ortamının aktif edilmesi

```
cp deploy/.env .env
```

```
docker-compose up
```

Docker içeriği

* mysql 5.7
* php7.2 
* nginx

Frontend dosyalarını derleme için aşağıdaki komutu vermeniz yeterlidir. 
Bu komut ile gerekli(birleştirme, küçültme) JS ve CSS işlemleri yapılarak; 
JS dosyaları public/js/app.js dosyasına dönüştürülür.
CSS dosyaları public/css/app.css dosyasına dönüştürülür.

Eğer geliştirme ortamında kullanmak isterseniz;
```
npm run dev
```

Eğer production ortamında kullanmak isterseniz;
```
npm run production
```

Testleri çalıştırmak için

```
docker-compose exec web pvendor/bin/phpunit
```

Migrate 

```
docker-compose exec web php artisan migrate
```

Örnek Dataları yüklemek için

```
docker-compose exec web php artisan db:seed
``` 

Uygulamanın Heroku Adresi:

https://dry-crag-56404.herokuapp.com/